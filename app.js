var 	express 				= require("express"),
		app						= express(),
		bodyParser   	 		= require("body-parser"),
		mongoose   			    = require("mongoose"),
   		passport 			    = require("passport");

const forumStartingImages = ["https://images.unsplash.com/photo-1525672716948-1f0bb9c49883?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=634&q=80",
"https://images.unsplash.com/photo-1605457004905-5fd559946933?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=634&q=80",
"https://images.unsplash.com/photo-1604326785227-2109b548e24b?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
"https://images.unsplash.com/photo-1592194996308-7b43878e84a6?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=634&q=80",
"https://images.unsplash.com/photo-1557166984-b00337652c94?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
"https://images.unsplash.com/photo-1557246565-8a3d3ab5d7f6?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
"https://images.unsplash.com/photo-1548546738-8509cb246ed3?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=634&q=80",
"https://images.unsplash.com/photo-1455970022149-a8f26b6902dd?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=648&q=80",
"https://images.unsplash.com/photo-1593095894307-c402e27bea27?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",];
const forumStartingTopics = ["Dogs", "Cats", "Weapons", "Video Games", "Travel", "Instagram"];
const forumStartingTitles = ["Does anyone ever?", "Hey what up guys I'm new here?", "How do I press the any key?", "What up guys", "Funny pictures thread", "asl?", "How much $ per month to live in Phillipines full time?", "How will this affect travel?", "What countries have the best winter scenery?"];
const forumStartingAuthors = ["Fakeuser123", "D_dog12", "Leetman", "Barrack Obama", "Donald Turmp", "Pepe", "Deeznuts123"];
const forumStartingReplies = ["Do not want", "Op's lame", "Bump", "GTFO", "what did you say to me? I'll have you know that I'm actually a scout sniper"];

let startingLikes = Math.floor(Math.random() * 1000);
let startingLikes2 = Math.floor(Math.random() * 1000);
let defaultLikes = 0;

function randomPost(selection){
	return Math.floor(Math.random() * selection.length); 
}

function increase(number){
	return number++;
}

mongoose.connect("mongodb://localhost/xyzforum");


app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/public"));


const votesDefault = 0;
const repliesDefault = "no replies";
const authorDefault = "pepeman_27";

let posts = [];

//=============
//	ROUTES
//=============

app.get("/", function(req,res){
	res.render("home", {
		
		startingImage: forumStartingImages[randomPost(forumStartingImages)],
		startingTopic: forumStartingTopics[randomPost(forumStartingTopics)],
		startingTitle: forumStartingTitles[randomPost(forumStartingTitles)],
		startingAuthor: forumStartingAuthors[randomPost(forumStartingAuthors)],
		startingReplies: forumStartingReplies[randomPost(forumStartingReplies)],
		startingLikes: startingLikes,
		startingImage2: forumStartingImages[randomPost(forumStartingImages)],
		startingTopic2: forumStartingTopics[randomPost(forumStartingTopics)],
		startingTitle2: forumStartingTitles[randomPost(forumStartingTitles)],
		startingAuthor2: forumStartingAuthors[randomPost(forumStartingAuthors)],
		startingReplies2: forumStartingReplies[randomPost(forumStartingReplies)],
		startingLikes2: startingLikes2,
		defaultLikes: defaultLikes,
		posts: posts
	});
});

app.get("/compose", function(req,res){
	res.render("compose");
});


app.get("/login", function(req,res){
	res.render("login");
});


app.get("/signup", function(req,res){
	res.render("signup");
});

app.get("/post", function(req, res){
	res.render("post");
})


app.post("/compose", function(req, res){
	const forumPost = {
		image: req.body.imageLink,
		topic: req.body.topicSelect,
		title: req.body.title,
		body: req.body.body
	}
	posts.push(forumPost);
	res.redirect("/");
});


app.get("/posts/:postID", function(req, res){
	const requestedTitle = req.params.postID;
	posts.forEach(function(post){
		const storedTitle = post.title;

		if(storedTitle === requestedTitle){
			res.render("post", {post:post})
		}
	})
})



//local setup
var port = process.env.PORT || 3000;
app.listen(port, function(){
	console.log("Project Server has started!");
});

