XYZ Company Forum (Final Project for PROG2053 WWW-teknologier Fall 2020)

Created by: Jason Ji

Objectives:
	-Basic front-end JavaScript manipulate data and fetch data from the server.
	-Basic back-endwith NodeJS, access to database.
	-File management.
	-Use of web components and other programming libraries.

Technologies and Libraries used:
	-html
	-CSS
	-javascript
	-jQuery
	-Bootstrap 4
	-NodeJS
	-Express
	-EJS
	-MongoDB
	-Mongoose
